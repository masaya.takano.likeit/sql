insert into item (item_id,item_name,item_price,category_id) values(1,'堅牢な机',3000,1);
insert into item (item_id,item_name,item_price,category_id) values(2,'生焼け肉',20,2);
insert into item (item_id,item_name,item_price,category_id) values(3,'すっきりわかるJava入門',20,2);
update item set item_price = 50 where item_id = 2;
update item set item_price = 3000 where item_id = 3;
update item set category_id = 3 where item_id = 3;
insert into item (item_id,item_name,item_price,category_id) values(4,'おしゃれな椅子',2000,1);
insert into item (item_id,item_name,item_price,category_id) values(5,'こんがり肉',500,2);
insert into item (item_id,item_name,item_price,category_id) values(6,'書き取りドリルSQL',2500,3);
insert into item (item_id,item_name,item_price,category_id) values(7,'ふわふわのベット',30000,1);
insert into item (item_id,item_name,item_price,category_id) values(8,'ミラノ風ドリア',300,2);
